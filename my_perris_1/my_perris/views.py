from django.shortcuts import render
from django.http import HttpResponse
from .models import Registro
from .models import Mascota
from django.shortcuts import redirect
# importar user
from django.contrib.auth.models import User
# sistema de autenticación
from django.contrib.auth import authenticate, logout, login as auth_login
from django.contrib.auth.decorators import login_required


def index(request):
    usuario = request.session.get('usuario',None)
    return render(request, 'index.html', {'name':'My Perris','userio':usuario})


def registro(request):
    return render(request, 'registro.html', {})


def login(request):
    return render(request, 'login.html', {})

def login_iniciar(request):
    username = request.POST.get('username','')
    contrasenia = request.POST.get('contrasenia','')
    user = authenticate(request,username=username, password=contrasenia)

    if user is not None:
        auth_login(request, user)
        request.session['usuario'] = user.username
        return redirect("mascotas")
    else:
        return render(request, 'login.html', {'Error': 'Usuario incorrecto'})


@login_required(login_url='login')
def cerrar_session(request):
    del request.session['usuario']
    logout(request)
    return redirect('login')



@login_required(login_url='login')
def mascotas(request):
    return render(request, 'mascotas.html', {'elementos': Mascota.objects.all()})


def adoptantes(request):
    return render(request, 'adoptantes.html', {'vistas': Mascota.objects.all()})



@login_required(login_url='login')
def crear(request):
    nombre = request.POST.get('nombre', '')
    run = request.POST.get('run', '')
    fecha_naci = request.POST.get('fecha_naci', '')
    email = request.POST.get('email', '')
    celular = request.POST.get('celular', 0)
    region = request.POST.get('region', '')
    ciudad = request.POST.get('ciudad', '')
    casa = request.POST.get('casa', '')
    username = request.POST.get('username','')
    contrasenia = request.POST.get('contrasenia', '')
    registro = Registro(nombre=nombre, run=run, fecha_naci=fecha_naci, email=email,
                        celular=celular, region=region, ciudad=ciudad, username=username,
                        casa=casa, contrasenia=contrasenia)
    registro.save()
    user = User.objects.create_user(username=username,password=contrasenia,email=email,first_name=nombre)
    user.save()
    return HttpResponse("Su cuenta se creo correctamente")

@login_required(login_url='login')
def crear_mascota(request):
    nombre = request.POST.get('nombre', '')
    raza = request.POST.get('raza', '')
    estado = request.POST.get('estado', '')
    imagen = request.FILES.get('imagen', '')
    comentario = request.POST.get('comentario', '')
    mascota = Mascota(nombre=nombre, raza=raza, estado=estado,
                      imagen=imagen, comentario=comentario)
    mascota.save()
    return redirect('mascotas')

@login_required(login_url='login')
def eliminar(request, id):
    mascota = Mascota.objects.get(pk=id)
    mascota.delete()
    return redirect('mascotas')

@login_required(login_url='login')
def modificar(request, id):
    nombre = request.POST.get('nombre', '')
    raza = request.POST.get('raza', '')
    estado = request.POST.get('estado', '')
    imagen = request.FILES.get('imagen', '')
    comentario = request.POST.get('comentario', '')
    mascota = Mascota.objects.get(pk=id)
    mascota.nombre = nombre
    mascota.raza = raza
    mascota.estado = estado
    mascota.imagen = imagen
    mascota.comentario = comentario
    mascota.save()
    #return HttpResponse("F")
    return redirect ('mascotas') 

@login_required(login_url='login')
def vista_modificar (request, id):
    mascota = Mascota.objects.get(id=id)
    print(mascota)
    return render(request, 'vista_modificar.html',{'mascota':mascota})
    #return HttpResponse("F")

 

