from django.apps import AppConfig


class MyPerrisConfig(AppConfig):
    name = 'my_perris'
